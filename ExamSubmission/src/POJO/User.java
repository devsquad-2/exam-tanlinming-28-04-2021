package POJO;

public class User {
	
	private String userLoginID;
	private String password;
	
	public String getUserLoginID() {
		return userLoginID;
	}
	public void setUserLoginID(String userLoginID) {
		this.userLoginID = userLoginID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
