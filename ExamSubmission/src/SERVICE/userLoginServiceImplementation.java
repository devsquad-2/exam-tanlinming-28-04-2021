package SERVICE;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

import DAO.userLoginDAOAuthenticationImplementation;
import DAO.userLoginDAOInterface;
import POJO.User;

public class userLoginServiceImplementation implements userLoginServiceInterface {
	
	userLoginDAOInterface refUserLoginDAOInterface = null;
	User refUser;
	
	boolean validOption = true;
	
	@Override
	public void callUserDAOImplementation() {
		
		refUser = new User();
		String userID = "";
		String password = "";
		Scanner sc = new Scanner(System.in);
		
		try {
			
			System.out.println("Enter User Name");
			userID = sc.next();
			validOption = true;
			
		} catch(InputMismatchException | NullPointerException e){
			
			System.out.println("Your input is invalid, please try again");
			validOption = false;
			
		}
	
		
		try {
			
			System.out.println("Enter User Password");
			password = sc.next();
			validOption = true;
			
		} catch(InputMismatchException | NullPointerException e){
			
			System.out.println("Your input is invalid, please try again");
			validOption = false;
			
		}
	
		
		refUser.setUserLoginID(userID);
		refUser.setPassword(password);
		
		System.out.println("Enter date in dd-mm-yyyy");
		String date = sc.next();
		Date date1 = new Date();
		Date date2 = new Date();

	    try {
			date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
			date2 = new SimpleDateFormat("dd-MM-yyyy").parse("30-04-2021");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    
	    long difference_In_Time = date2.getTime() -date1.getTime();
	    
	    long difference_In_Days = (difference_In_Time/(1000* 60 * 60 * 24)) % 365;
	    
	    
		refUserLoginDAOInterface = new userLoginDAOAuthenticationImplementation();
		if(refUserLoginDAOInterface.userLoginAuthentication(refUser)) {
			System.out.println("Sucessfully Login");	
		}
		if(difference_In_Days < 0) {
			float payableAmount = 80.00f;
			payableAmount = (float) (payableAmount * 0.05 + payableAmount);
			System.out.println("Payment Due Date 30-Apr-2021 so you are fined and the Total Payable Amount: " + payableAmount);
		} else {
			System.out.println("Payment Due Date 30-Apr-2021 so you are not fined and the Total Payable Amount: " + 80.00);
		}
	}
}
